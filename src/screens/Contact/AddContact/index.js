import React, {Component} from 'react';
import {connect} from 'react-redux';
import AddContactData from '../../../components/Contact/AddContactData/index';
import {postAddContact} from '../../../actions/ContactAction';
import swal from 'sweetalert';

const mapStateToProps (= (state) =>) ({
    postAddContactDataLoading: state.ContactReducer.postAddContactDataLoading,
    postAddContactData: state.ContactReducer.postAddContactData,
    postAddContactDataError: state.ContactReducer.postAddContactDataError,
})

class AddContact extends Component {
  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate = (prevProps, prevState) => {
    const {postAddContactData} = this.props;
    if (postAddContactData !== "" && prevProps.postAddContactData !== postAddContactData) {
      window.location.replace('/');
    }
  };

  _confirmPostAddContact = (data) => {
    swal({
          title: "Submit Contact",
          text: "Are you sure save this contact?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((value) => {
          if (value) {
            this._postAddContact(data);
          }
      }); 
  };

  _postAddContact = (data) => {
    const {postAddContact} = this.props;
    postAddContact(
      data.firstName,
      data.lastName,
      data.age,
      data.photo
    );
  };

  handleSubmit = (data) => {        
    this._confirmPostAddContact(data);
  };

  render = () => {
    return (
      <div>
        <AddContactData onSubmit={this.handleSubmit}/>
      </div>
    );
  };
};

export default (connect(mapStateToProps, {postAddContact})(AddContact));