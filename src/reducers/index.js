import { combineReducers } from "redux";
import { reducer as form } from 'redux-form';
// import { reducer as toastr } from 'react-redux-toastr';

import ContactReducer from "./ContactReducer"

const rootReducer = combineReducers({
    ContactReducer,
    form,
    // toastr,
});

export default rootReducer;
